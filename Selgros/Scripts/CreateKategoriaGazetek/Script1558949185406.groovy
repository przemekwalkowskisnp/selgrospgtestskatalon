import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.authenticate('https://qasspg.clouddc.eu/', 'nowickic', '%23spG4pp%40', 50)
WebUI.click(findTestObject('Page_Kokpit/a_Panel administratora (1)'))

WebUI.click(findTestObject('Object Repository/Page_Panel administratora/a_Dodaj now grup gazetek'))

WebUI.setText(findTestObject('Object Repository/Page_Nowa grupa gazetek/input_Nazwa_PromoCatalogueConfigurationGroupName'), 
    'Toster')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Nowa grupa gazetek/select_StandardowaRegionalnaKatalog'), 
    'd8fafa1e-25b2-41bb-855f-22defc2e623e', true)

WebUI.click(findTestObject('Object Repository/Page_Nowa grupa gazetek/button_Zapisz'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Lista grup gazetek/div_Operacja wykonana poprawnie'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Lista grup gazetek/td_Toster'))

WebUI.closeBrowser()

