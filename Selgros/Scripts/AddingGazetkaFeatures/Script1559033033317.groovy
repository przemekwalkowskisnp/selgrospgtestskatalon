import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.authenticate('https://qasspg.clouddc.eu/', 'nowickic', '%23spG4pp%40', 50)

WebUI.click(findTestObject('Object Repository/Page_Kokpit/a_2019 (1)'))

WebUI.click(findTestObject('Object Repository/Page_Kokpit/a_ Dodaj wydanie (1)'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Tworzenie nowej edycji/select_Wybierz podtypAlkohole mocnee-sklep'), 
    '9aaa8c94-33f0-4bac-b60c-3cfd4dfd0c20', true)

WebUI.click(findTestObject('Object Repository/Page_Tworzenie nowej edycji/button_Utwrz i przejd do edycji cech wydania (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Edytuj wydanie/input_Strony FOOD_PromoCatalogueFoodPageCount (1)'), 
    '25')

WebUI.setText(findTestObject('Object Repository/Page_Edytuj wydanie/input_Strony NON-FOOD_PromoCatalogueNonFoodPageCount (1)'), 
    '15')

WebUI.click(findTestObject('Object Repository/Page_Edytuj wydanie/input_FOOD  NONFOOD_PromoCatalogueLayoutDic (1)'))

WebUI.click(findTestObject('Object Repository/Page_Edytuj wydanie/div_Numer wydania_col-md-7'))

WebUI.setText(findTestObject('Object Repository/Page_Edytuj wydanie/input_Numer wydania_PromoCatalogueCatalogueNumber'), 
    '123')

WebUI.click(findTestObject('Object Repository/Page_Edytuj wydanie/button_Zapisz to wydanie (1)'))

WebUI.click(findTestObject('Object Repository/Page_Kokpit/div_Strony dla bran (1)'))

WebUI.selectOptionByValue(findTestObject('Page_Strony dla bran/select_Wybierz102030404450556075808590100102105110200300310320400410555 (1)'), 
    'f0a23d38-5a0e-4ea2-a6f5-2a53d462e1d5', true)

WebUI.selectOptionByValue(findTestObject('Page_Strony dla bran/select_Wybierz102030404450556075808590100102105110200300310320400410555_1 (1)'), 
    '7f825e3b-d3ef-44e3-af41-f666126bb847', true)

WebUI.click(findTestObject('Page_Strony dla bran/button_Zapisz (1)'))

WebUI.verifyElementPresent(findTestObject('Page_Strony dla bran/div_Operacja wykonana poprawnie'), 0)

WebUI.click(findTestObject('Page_Strony dla bran/div_Artykuy (1)'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/button_23 (1)'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/label_Wszystkie'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/label_10'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/label_20'))

WebUI.click(findTestObject('Page_Artykuy/button_Filtruj'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input_DKZ redni_Articles0IsInPromoCatalogue (1)'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input_redni_Articles1IsInPromoCatalogue (1)'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input_redni_Articles2IsInPromoCatalogue (1)'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/button_Zapisz (1)'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/div_Operacja wykonana poprawnie (1)'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/span_Warunki handlowe'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/a_Edytuj (1)'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Ustaw domylny_BusinessTermDetailsDiscountPeriodForAll'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Baza upustu_BusinessTermDetailsSuppliers0BtSupplierDiscountBase'), 
    '3')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Upust_BusinessTermDetailsSuppliers0BtSupplierDiscountValue'), 
    '12')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Rabaty progowe_BusinessTermDetailsSuppliers0BtSupplierHasSupplierThresholdDiscounts'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Upust_BusinessTermDetailsSuppliers0BtSupplierThresholdDiscounts0DiscountPercentage'), 
    '12')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Dodaj rabat'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_rabatu promocyjnego_BusinessTermDetailsSuppliers0BtSupplierThresholdDiscounts1Units'), 
    '2')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_ _BusinessTermDetailsSuppliers0BtSupplierThresholdDiscounts1DiscountValue'), 
    '1')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Dodaj rabat'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Cena kocowa_BusinessTermDetailsSuppliers0BtSupplierThresholdDiscounts2FinalPrice'), 
    '1')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Ustal cene koncesyjna_BusinessTermDetailsBusinessTermIsPriceOfTheDay'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_rabatu promocyjnego_BusinessTermDetailsSuppliers0BtSupplierThresholdDiscounts2Units'), 
    '3')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Promocyjna_BusinessTermDetailsBusinessTermOrdinaryPrice (1)'), 
    '0,75')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Dodatkowe informacje_BusinessTermDetailsBusinessTermHasCommentsForCoordination'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/textarea_Uwagi dla koordynacji_BusinessTermDetailsBusinessTermCommentsForCoordination (1)'), 
    'Uwagi')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Zapisz (1)'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/a_Edytuj_1 (1)'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Promocja ze stanu_BusinessTermDetailsSuppliers0BtSupplierIsNakedPrice'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Baza upustu_BusinessTermDetailsSuppliers0BtSupplierDiscountBase'), 
    '2')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Uwagi dla koordynacji_BusinessTermDetailsBusinessTermHasCommentsForExposition (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/textarea_Uwagi dla ekspozycji_BusinessTermDetailsBusinessTermCommentsForExposition (1)'), 
    'Uwagi')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Promocyjna_BusinessTermDetailsBusinessTermOrdinaryPrice (1)'), 
    '2,190')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Zapisz (1)'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/a_Edytuj_2 (1)'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Moliwo domwienia w okresie promocji_BusinessTermDetailsSuppliers0BtSupplierIsPromotionFromStock'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Baza upustu_BusinessTermDetailsSuppliers0BtSupplierDiscountBase'), 
    '1,5')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input__BusinessTermDetailsSuppliers0BtSupplierIsDiscountPercentage'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input__BusinessTermDetailsSuppliers0BtSupplierDiscountAmount'), 
    '0,5')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/div_Promocyjna                                                        z (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Promocyjna_BusinessTermDetailsBusinessTermOrdinaryPrice (1)'), 
    '1')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Rabaty progowe_BusinessTermDetailsBusinessTermHasThresholdDiscounts'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Warunki handlowe/div_Pole Baza upustu musi by z zakresu od 0 do 6'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Baza upustu_BusinessTermDetailsSuppliers0BtSupplierDiscountBase'), 
    '2')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Dodaj rabat_1'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Upust_BusinessTermDetailsThresholdDiscounts0DiscountPercentage'), 
    '12')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Dodaj rabat_2'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_procentowy_BusinessTermDetailsThresholdDiscounts1DiscountTypeDic'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_ _BusinessTermDetailsThresholdDiscounts1DiscountValue'), 
    '0,30')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Dodaj rabat_3'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_kwotowy_BusinessTermDetailsThresholdDiscounts2DiscountTypeDic'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Cena kocowa_BusinessTermDetailsThresholdDiscounts2FinalPrice'), 
    '0,1')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Zapisz (1)'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/span_Potwierdzenia promocyjne'))

WebUI.click(findTestObject('Object Repository/Page_Lista potwierdze promocyjnych/button_Zapisz'))

WebUI.click(findTestObject('Object Repository/Page_Lista potwierdze promocyjnych/span_Ukad   stron'))

WebUI.closeBrowser()

