<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Nazwa</name>
   <tag></tag>
   <elementGuidId>36183de5-e823-4f5a-ace6-6ed88c16c229</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='SaveUpdateTarget']/div/div/div[3]/div[4]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tab-content</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                            

                            
                            
                                
                                    
                                        Nazwa dostawcy : 
                                        Lieferant: 40313
                                        
                                        
                                    
                                    
                                        %NKV = 10,15 
                                    


                                
                                    
                                        
                                            Poddostawca : 
                                            

                                                0

                                            
                                        
                                    
                                
                                    
                                        Rezerwacja:
                                         szt.
                                    
                                    
                                        
                                        Możliwość domówienia w okresie promocji
                                    
                                
                                
                                    
                                        
                                        Promocja ze stanu?
                                    
                                
                                
                                    
                                        
                                        Cena bez rabatu promocyjnego
                                        (zaznacz jeśli produkt nie ma dodatkowego rabatu promocyjnego)
                                    
                                
                                
                                    
                                        Baza upustu:
                                        
                                        Upust:

                                        
                                        
                                        %
                                        |
                                        
                                        
                                        zł
                                    

                                    

                                        Cena zakupu promocyjnego: (EKRK)*:
                                         PLN
                                        |
                                        EK NN:
                                        0,000 PLN
                                    
                                
                                



                                    Cena zakupu standardowa :


                                    
                                        
                                    
                                
                                
                                    
                                    
                                    
                                    
                                    
                                    


                                    

                                            

                                                

                                                        

                                                                
                                                                    R1
                                                                    
                                                                        2,000%
                                                                        
                                                                    
                                                                

                                                                
                                                                    R2
                                                                    
                                                                        7,900%
                                                                        
                                                                    
                                                                






                                                        
                                                
                                            

                                        
                                            

                                                




                                                
                                                    
                                                        
                                                            
                                                                EK LI 
                                                            
                                                        
                                                        
                                                             PLN
                                                            
                                                            
                                                        
                                                    
                                                    
                                                        
                                                            EK RK 
                                                        
                                                        
                                                             PLN
                                                            
                                                            
                                                        
                                                    
                                                    
                                                        
                                                            EK NN 
                                                                                                                    
                                                        


                                                             PLN
                                                            
                                                            
                                                        

                                                    

                                                

                                                
                                                
                                            

                                        


                                        

                                            
                                                
                                                    Brak przyszłych cen zakupu z &quot;P&quot;
                                                
                                            
                                            
                                                Warunki dostawy zostały  potwierdzone przez dostawcę
                                            


                                        

                                    







                                


                                
                                    
                                        Rabaty progowe:
                                        
                                        Ustal rabaty progowe
                                    
                                    
                                        Dodaj rabat
                                    
                                
                            
                        
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SaveUpdateTarget&quot;)/div[@class=&quot;validationGroup&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;panel panel-default&quot;]/div[@class=&quot;panel-body no-padding&quot;]/div[@class=&quot;tab-content&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='SaveUpdateTarget']/div/div/div[3]/div[4]/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Warunki dla dostawcy'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Warunki zakupu i dostawy'])[1]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
