<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Selgros</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f2bcfbff-24e7-45d4-930f-9df4789ae651</testSuiteGuid>
   <testCaseLink>
      <guid>4975a51f-c07f-4b56-9d4b-5e17446a0ba3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CreateGazetka</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06e27de0-c9c4-4d59-9c2e-0380a2d73256</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CreateKategoriaGazetek</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e054484-2bab-40a2-93b2-72ac08e9385b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CreateTermin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d5066f4-830d-406b-87a2-8820a61b0096</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddingGazetkaFeatures</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
