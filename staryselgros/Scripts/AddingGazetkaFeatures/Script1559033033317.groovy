import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://qasspg.clouddc.eu/')

WebUI.click(findTestObject('Object Repository/Page_Kokpit/a_2019'))

WebUI.click(findTestObject('Object Repository/Page_Kokpit/a_ Dodaj wydanie'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Tworzenie nowej edycji/select_Wybierz podtypAlkohole mocnee-sklep Standardpromocja NFStandardpromocja FStandardpromocja FNFStandardpromocja NFSuperoferta FWina Polskie'), 
    '9aaa8c94-33f0-4bac-b60c-3cfd4dfd0c20', true)

WebUI.click(findTestObject('Object Repository/Page_Tworzenie nowej edycji/button_Utwrz i przejd do edycji cech wydania'))

WebUI.setText(findTestObject('Object Repository/Page_Edytuj wydanie/input_Strony FOOD_PromoCatalogueFoodPageCount'), '25')

WebUI.setText(findTestObject('Object Repository/Page_Edytuj wydanie/input_Strony NON-FOOD_PromoCatalogueNonFoodPageCount'), 
    '15')

WebUI.click(findTestObject('Object Repository/Page_Edytuj wydanie/input_FOOD  NONFOOD_PromoCatalogueLayoutDic'))

WebUI.click(findTestObject('Object Repository/Page_Edytuj wydanie/button_Zapisz to wydanie'))

WebUI.click(findTestObject('Object Repository/Page_Kokpit/div_Strony dla bran'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Strony dla bran/select_Wybierz102030404450556075808590100102105110200300310320400410555'), 
    'f0a23d38-5a0e-4ea2-a6f5-2a53d462e1d5', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Strony dla bran/select_Wybierz102030404450556075808590100102105110200300310320400410555_1'), 
    '7f825e3b-d3ef-44e3-af41-f666126bb847', true)

WebUI.click(findTestObject('Object Repository/Page_Strony dla bran/button_Zapisz'))

WebUI.click(findTestObject('Object Repository/Page_Strony dla bran/div_Artykuy'))

WebUI.click(findTestObject('Object Repository/Page_Strony dla bran/div_Operacja wykonana poprawnie'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/button_23'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/button_Filtruj'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input_DKZ redni_Articles0IsInPromoCatalogue'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/div_concat(html1body1divclass  blockUI blockOverlay  )_blockUI blockOverlay'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input_redni_Articles1IsInPromoCatalogue'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input_redni_Articles2IsInPromoCatalogue'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/input_redni_Articles3IsInPromoCatalogue'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/button_Zapisz'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/div_Operacja wykonana poprawnie'))

WebUI.click(findTestObject('Object Repository/Page_Artykuy/div_Warunki handlowe'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/a_Edytuj'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input__BusinessTermDetailsSuppliers0BtSupplierFinalPrice'), 
    '4,000')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Plan sprzeday_BusinessTermDetailsBusinessTermSalesPlan'), 
    '1')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Promocyjna_BusinessTermDetailsBusinessTermOrdinaryPrice'), 
    '5,000')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Zapisz'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/div_Sukces                                        Warunki handlowe zostay zapisane'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/a_Edytuj_1'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input__BusinessTermDetailsSuppliers0BtSupplierFinalPrice'), 
    '2,000')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Plan sprzeday_BusinessTermDetailsBusinessTermSalesPlan'), 
    '1')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Promocyjna_BusinessTermDetailsBusinessTermOrdinaryPrice'), 
    '2,000')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Uwagi dla koordynacji_BusinessTermDetailsBusinessTermHasCommentsForExposition'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/textarea_Uwagi dla ekspozycji_BusinessTermDetailsBusinessTermCommentsForExposition'), 
    'Uwaga 123')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Zapisz'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/div_Sukces                                        Warunki handlowe zostay zapisane'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/a_Edytuj_2'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input__BusinessTermDetailsSuppliers0BtSupplierFinalPrice'), 
    '2,5')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Plan sprzeday_BusinessTermDetailsBusinessTermSalesPlan'), 
    '1')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/div_Promocyjna                                                        z'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Promocyjna_BusinessTermDetailsBusinessTermOrdinaryPrice'), 
    '3')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/label_Uwagi dla koordynacji'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/textarea_Uwagi dla koordynacji_BusinessTermDetailsBusinessTermCommentsForCoordination'), 
    'uwaga 321')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Zapisz'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/div_Sukces                                        Warunki handlowe zostay zapisane'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/a_Edytuj_3'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input__BusinessTermDetailsSuppliers0BtSupplierFinalPrice'), 
    '1,900')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/div_Plan sprzeday'))

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Plan sprzeday_BusinessTermDetailsBusinessTermSalesPlan'), 
    '1')

WebUI.setText(findTestObject('Object Repository/Page_Warunki handlowe/input_Promocyjna_BusinessTermDetailsBusinessTermOrdinaryPrice'), 
    '2,000')

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Uwagi dla ekspozycji_BusinessTermDetailsBusinessTermHasSelectedPlants'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/input_Zaznaczodznacz wszystkie_BusinessTermDetailsSelectedPlants'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/button_Zapisz'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/div_Sukces                                        Warunki handlowe zostay zapisane'))

WebUI.click(findTestObject('Object Repository/Page_Warunki handlowe/div_Potwierdzenia promocyjne'))

WebUI.click(findTestObject('Object Repository/Page_Lista potwierdze promocyjnych/div_Ukad   stron'))

WebUI.closeBrowser()

