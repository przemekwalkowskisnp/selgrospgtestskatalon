import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.authenticate('https://qasspg.clouddc.eu/', 'nowickic', '%23spG4pp%40', 50)

WebUI.click(findTestObject('Page_Kokpit/a_Panel administratora (3)'))

WebUI.click(findTestObject('Page_Panel administratora/a_Dodaj nowy termin publikacji (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Tworzenie nowego terminu publikacji/input_Numer terminu publikacji_EditionEditionNumber (1)'), 
    '123')

WebUI.setText(findTestObject('Object Repository/Page_Tworzenie nowego terminu publikacji/input_Data pocztkowa_EditionEditionFrom (1)'), 
    '2019-06-28')

WebUI.click(findTestObject('Object Repository/Page_Tworzenie nowego terminu publikacji/div_Bdy na formularzu            Numer terminu publikacji'))

WebUI.setText(findTestObject('Object Repository/Page_Tworzenie nowego terminu publikacji/input_Data kocowa_EditionEditionTo'), 
    '2019-06-30')

WebUI.click(findTestObject('Object Repository/Page_Tworzenie nowego terminu publikacji/div_Bdy na formularzu            Numer terminu publikacji'))

WebUI.click(findTestObject('Object Repository/Page_Tworzenie nowego terminu publikacji/div_Czas trwania'))

WebUI.setText(findTestObject('Object Repository/Page_Tworzenie nowego terminu publikacji/input_Czas trwania_EditionDuration'), 
    '2')

WebUI.click(findTestObject('Object Repository/Page_Tworzenie nowego terminu publikacji/button_Zapisz'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Terminy publikacji/div_Operacja wykonana poprawnie'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Terminy publikacji/td_123'))

WebUI.closeBrowser()

