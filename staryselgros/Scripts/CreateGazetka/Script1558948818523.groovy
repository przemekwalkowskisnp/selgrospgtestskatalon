import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.security.UserAndPassword as UserAndPassword
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.authenticate('https://qasspg.clouddc.eu/', 'nowickic', '%23spG4pp%40', 50)

WebUI.click(findTestObject('Page_Kokpit/a_Panel administratora'))

WebUI.click(findTestObject('Object Repository/Page_Panel administratora/a_Dodaj now gazetk'))

WebUI.setText(findTestObject('Object Repository/Page_Nowa gazetka/input_Nazwa gazetki_PromoCatalogueConfigurationPromoName'),
	'Tost')

WebUI.setText(findTestObject('Object Repository/Page_Nowa gazetka/input_Domylna liczba stron food_PromoCatalogueConfigurationMaxFoodPagesCount'),
	'25')

WebUI.setText(findTestObject('Object Repository/Page_Nowa gazetka/input_Domylna liczba stron non food_PromoCatalogueConfigurationMaxNonFoodPagesCount'),
	'15')

WebUI.click(findTestObject('Object Repository/Page_Nowa gazetka/button_Zapisz'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Lista gazetek/div_Operacja wykonana poprawnie'))

WebUI.setText(findTestObject('Object Repository/Page_Lista gazetek/input_Nazwa gazetki_StatePromoName'), 'Tost')

WebUI.click(findTestObject('Object Repository/Page_Lista gazetek/input_Nazwa gazetki_btn btn-xs'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Lista gazetek/td_Tost'))

WebUI.click(findTestObject('Object Repository/Page_Lista gazetek/button_Usu'))

WebUI.closeBrowser()