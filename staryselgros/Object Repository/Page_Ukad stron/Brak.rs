<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Brak</name>
   <tag></tag>
   <elementGuidId>226f5c71-7e00-4b97-983a-db5f178e33d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Uwagi do strony'])[1]/following::td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-sortable noArticles</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-rasterid</name>
      <type>Main</type>
      <value>f9b81c18-ad2e-4089-9d5b-3a5d3e243c66</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-rasternumber</name>
      <type>Main</type>
      <value>1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                            
                                                            
                                    S1
                                
                                    Artykuły
                                    
                                        Brak artykułów
                                    
                                                        
                                                        
                                                        
                                                        
                                                        
                                                            warunki handlowe
                                                        
                                                        
                                                            projekt strony
                                                        
                                                        
                                                            Dodaj artykuł
                                                        
                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/form[@class=&quot;form-inline&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;validationGroup&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;form-inline&quot;]/div[@class=&quot;panel panel-default panel-tabs&quot;]/div[@class=&quot;panel-body no-padding&quot;]/div[@class=&quot;raster-products&quot;]/table[@class=&quot;layoutView&quot;]/tbody[1]/tr[1]/td[@class=&quot;ui-sortable noArticles&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Uwagi do strony'])[1]/following::td[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tytuł strony'])[1]/following::td[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td</value>
   </webElementXpaths>
</WebElementEntity>
